
var interval = setInterval(function () {
  console.log('Class shifting');

  var removed = 0;
  var elements = document.getElementsByClassName('encrypted-content');
  for (var y=0; y < elements.length; y++) {
    removed = 1;
    elements[y].classList.add('npc-saved-apg');
    elements[y].classList.remove('encrypted-content');
  }

  if (removed == 1) {
    clearInterval(interval);
  }
}, 0);

