var cleanse_targets = {
  'postbulletin.com': [
    {
      'type': 'hide_class',
      'value': 'subscription-required'
    },
    {
      'type': 'remove_class',
      'value': 'hide'
    },
    {
      'type': 'remove_all_styles_identified_by_class',
      'value': 'redacted-overlay'
    }
  ],
  'startribune.com': [
    {
      'type': 'remove_class',
      'value': 'Text_Body'
    },
    {
      'type': 'remove_class',
      'value': 'ReactModal__Body--open'
    },
    {
      'type': 'hide_class',
      'value': 'ReactModalPortal'
    }
  ],
  'apg-wi.com': [
    {
      'type': 'apg_decrypt',
      'value': 'npc-saved-apg'
    },
    {
      'type': 'apg_decrypt',
      'value': 'encrypted-content'
    },
    {
      'type': 'remove_all_styles_identified_by_class',
      'value': 'npc-saved-apg'
    },
    {
      'type': 'remove_all_styles_identified_by_class',
      'value': 'encrypted-content'
    },
    {
      'type': 'hide_class',
      'value': 'access-offers-modal'
    },
    {
      'type': 'hide_class',
      'value': 'modal-backdrop'
    },
    {
      'type': 'remove_class',
      'value': 'modal-open'
    },
    {
      'type': 'remove_attribute_identified_by_id',
      'value': ['asset-content', 'hidden']
    }
  ]
}

function decodeEntities(encodedString) {
  var textArea = document.createElement('textarea');
  textArea.innerHTML = encodedString;
  return textArea.value;
}

function unscramble(sInput) {
    var sOutput = '';
    for (var i = 0, c = sInput.length; i < c; i++) {
        var nChar = sInput.charCodeAt(i);
        if (nChar >= 33 && nChar <= 126) {
            sTmp = String.fromCharCode(33 + (((nChar - 33) + 47) % 94));
            sOutput += sTmp
        } else {
            sOutput += sInput.charAt(i)
        }
    }
    return sOutput
};

function cleansePapers()
{
  // Turn the domain into something a little less specific
  var domain_pieces = window.location.hostname.split(/\./);
  var domain = domain_pieces[domain_pieces.length - 2] + '.' + domain_pieces[domain_pieces.length - 1] 

  // Start attacking bad elements on the page
  for (var i=0; i < cleanse_targets[domain].length; i++) {
    var rule = cleanse_targets[domain][i];

    if (rule['type'] == 'apg_decrypt' ) {
      var elements = document.getElementsByClassName(rule['value']);
      for (var y=0; y < elements.length; y++) {
        if (elements[y].getAttribute("decrypted") != "1") {
          elements[y].setAttribute("decrypted", "1");
          elements[y].innerHTML = unscramble(decodeEntities(elements[y].innerHTML))
        }
      }
    }
    else if (rule['type'] == 'hide_class') {
      var elements = document.getElementsByClassName(rule['value']);
      for (var y=0; y < elements.length; y++) {
        elements[y].style.visibility = "none";
        elements[y].style.display = "none";
      }
    }
    else if (rule['type'] == 'remove_class') {
      var elements = document.getElementsByClassName(rule['value']);
      for (var y=0; y < elements.length; y++) {
        elements[y].classList.remove(rule['value']);
      }
    }
    else if (rule['type'] == 'remove_all_styles_identified_by_class') {
      var elements = document.getElementsByClassName(rule['value']);
      for (var y=0; y < elements.length; y++) {
        elements[y].removeAttribute('style');
      }
    }
    else if (rule['type'] == 'remove_attribute_identified_by_id') {
      var element = document.getElementById(rule['value'][0]);
      element.removeAttribute(rule['value'][1]);
    }
  }
}

var observer = new MutationObserver(function(mutations) {
  console.log('Mutation!');
  cleansePapers();
});

var targetNode = document.body;
observer.observe(targetNode, {attributes: true, childList: true, subtree: true});

console.log('Newspaper Cleanser started');

cleansePapers();
